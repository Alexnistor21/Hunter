import cocos
from cocos.actions import MoveBy, Repeat, Reverse
from cocos.director import director
import pyglet
from pyglet.window import key
from pyglet.window.key import symbol_string


# class Mover(cocos.actions.Move):
#     def step(self, dt):
#         super().step(dt)
#         vel_x = (keyboard[key.RIGHT] - keyboard[key.LEFT]) * 500
#         vel_y = (keyboard[key.UP] - keyboard[key.DOWN]) * 500
#         self.target.velocity = (vel_x, vel_y)
#         scroller.set_focus(self.target.x, self.target.y)
#         # image = getattr(self.target, 'image', 0)
#         img = pyglet.image.load("sprite/mario.png")
#         img_grid = pyglet.image.ImageGrid(img, 3, 28, item_width=13, item_height=18)
#         self.anim_walking = pyglet.image.Animation.from_image_sequence(img_grid[4:6], 0.1, loop=True)
#         # setattr(self.target, 'image', self.anim_walking)
#
#         def on_key_press(self, key, modifiers):
#             print("key pressed" + key + modifiers)
#             if symbol_string(key) == "RIGHT":
#                 self.spr.image = cocos.sprite.Sprite(self.anim_walking)
#
#             if symbol_string(key) == "LEFT":
#                 self.spr.do(Reverse(self.anim_walking))



class Mainchar(cocos.layer.ScrollableLayer):
    def __init__(self):
        super().__init__()
        img = pyglet.image.load("sprite/mario.png")
        img_grid = pyglet.image.ImageGrid(img, 28, 3, item_width=15, item_height=18)
        anim_walking = pyglet.image.Animation.from_image_sequence(img_grid[1:], 1, loop=True)
        # self.anim_stopped = pyglet.image.Animation.from_image_sequence(img_grid[3:3], 1)
        self.spr = cocos.sprite.Sprite(anim_walking)
        self.spr.position = 50, 50
        self.spr.velocity = (0, 0)
        # self.spr.do(Mover())
        self.add(self.spr)



    # def on_key_press(self, key, modifiers):
    #     print("key pressed" + key + modifiers)
    #     if symbol_string(key) == "RIGHT":
    #         self.spr.image = cocos.sprite.Sprite(self.anim_walking)
    #
    #     if symbol_string(key) == "LEFT":
    #         self.spr.do(Reverse(self.anim_walking))


class BackgroundLayer():
    def __init__(self):
        bg = cocos.tiles.load("C:\\Users\\Dell\\hunter\\map\\map_03.tmx")
        self.layer1 = bg["background"]
        self.layer2 = bg["graphics"]
        # self.layer3 = bg["ground"]
        # self.layer4 = bg["pipes"]
        # self.layer5 = bg["coins"]
        # self.layer6 = bg["bricks"]


if __name__ == "__main__":
    director.init(width=1280, height=720, caption="Hunter project")

    keyboard = key.KeyStateHandler()
    director.window.push_handlers(keyboard)

    mainchar = Mainchar()
    bg_layer = BackgroundLayer()

    scroller = cocos.layer.ScrollingManager()
    scroller.add(bg_layer.layer1)
    scroller.add(bg_layer.layer2)
    # scroller.add(bg_layer.layer3)
    # scroller.add(bg_layer.layer4)
    # scroller.add(bg_layer.layer5)
    # scroller.add(bg_layer.layer6)
    scroller.add(mainchar)

    main_scene = cocos.scene.Scene()
    main_scene.add(scroller)
    director.run(main_scene)
